/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';

// poker-odds
import { CardGroup, OddsCalculator } from 'poker-odds-calculator';

// components
import Board from 'components/Board';
import PlayerOddsItem from 'components/PlayerOddsItem';
import FormControlWrapper from 'components/FormControlWrapper';

export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);

        this.state = {
            qPlayerList: '',
            board: '',
            playerList: ['asks', '3d3c'],
        };

        // bind
        this.handleBoardChange = this.handleBoardChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleBoardChange = event => {
        this.setState({ board: event.target.value });
    };

    handleChange = event => {
        this.setState({ qPlayerList: event.target.value });
    };

    renderResults() {
        const { board, qPlayerList } = this.state;
        const playerList = qPlayerList.split(' ');

        let cgBoard;

        let foundError = false;
        let errorMessage = '';

        // data model
        let calcResponse = {
            equities: [],
            handranks: {},
        };

        // validate board
        try {
            cgBoard = CardGroup.fromString(board);
        } catch (error) {
            foundError = true;
            errorMessage = error.message;
            return false;
        }

        // attempt assign hole cards to players
        const cardGroupList = playerList.map((holeCards) => {
            try {
                return CardGroup.fromString(holeCards);
            } catch (error) {
                foundError = true;
                errorMessage = '';
                return false;
            }
        });

        // check error
        if (foundError) {
            return (
              <div>{errorMessage}</div>
            );
        }

        // validate hole cards
        cardGroupList.forEach((item) => {
            // number of cards in each players hand
            if (item.length < 2) {
                foundError = true;
                errorMessage = 'not valid hole cards';
            }
        });

        // check error
        if (foundError) {
            return (
              <div>{errorMessage}</div>
            );
        }

        // attempt calculation
        try {
            calcResponse = OddsCalculator.calculate(cardGroupList, cgBoard);
        } catch (error) {
            foundError = true;
            errorMessage = error.message;
        }

        // check error
        if (foundError) {
            return (
              <div>{errorMessage}</div>
            );
        }

        // everything is valid - render odds
        const { equities, handranks } = calcResponse;

        const renderOdds = equities.map((item, index) => {
            const player = `Player ${index + 1}`;
            const holeCards = playerList[index];
            const equity = equities[index];

            const handRank = handranks[index].rank;
            console.log('handRank', handRank);

            const odds = `${equity.getEquity()}%`;
            const tiePercent = `${equity.getTiePercentage()}%`;
            const key = `${index}-${Math.floor(Math.random() * Math.floor(100))}`;

            console.log('tiePercent', tiePercent);

            return (
              <PlayerOddsItem
                key={key}
                name={player}
                holeCards={holeCards}
                odds={odds}
              />
            );
        });

        return (
          <div>
            <h2>Results</h2>

            {renderOdds}
          </div>
        );
    }

    renderForm() {
        const { board, qPlayerList } = this.state;

        // player hands
        const label = 'Player Hands';
        const placeholder = 'asks 3d3c';

        // board
        const labelBoard = 'Board';
        const placeholderBoard = '2d9s4sqh';

        return (
          <div>
            <h2>Enter Info</h2>

            <FormControlWrapper
              className={'PlayerHands'}
              label={label}
              value={qPlayerList}
              placeholder={placeholder}
              handleChange={this.handleChange}
            />

            <FormControlWrapper
              className={'Board'}
              label={labelBoard}
              value={board}
              placeholder={placeholderBoard}
              handleChange={this.handleBoardChange}
            />
          </div>
        );
    }

    render() {
        const { board } = this.state;

        const renderForm = this.renderForm();
        const renderResults = this.renderResults();

        return (
          <div>
            {renderForm}
            <br />
            {renderResults}
            <Board boardCards={board} />
          </div>
        );
    }
}
