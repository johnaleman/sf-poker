import React from 'react';
import PropTypes from 'prop-types';

// style
import styled from 'styled-components';
import style from './style';

// ui
import Item from 'components/Item';

const StylesList = styled.div`${style}`;

const propTypes = {
  // list: PropTypes.arrayOf(PropTypes.object),
  handleDeleteClick: PropTypes.func,
  handleEditClick: PropTypes.func,
  handleItemClick: PropTypes.func,
};
const defaultProps = {
  handleDeleteClick: () => {},
  handleEditClick: () => {},
  handleItemClick: () => {},
};

function AdminList(props) {
  const handleDeleteClick = (item) => {
    props.handleDeleteClick(item);
  };

  const handleEditClick = (item) => {
    props.handleEditClick(item);
  };

  const handleItemClick = (item) => {
    props.handleItemClick(item);
  };

  const renderServiceList = () => {
    const { list } = props;

    // validation
    if (!list) {
      return;
    }

    return list.map((item) => {
      return (
        <Item
            key={item.id}
            item={item}
            handleDeleteClick={handleDeleteClick}
            handleEditClick={handleEditClick}
            handleItemClick={handleItemClick}
        />
      );
    });
  };

  return (
    <StylesList>
      <div>{renderServiceList()}</div>
    </StylesList>
  );
}

AdminList.propTypes = propTypes;
AdminList.defaultProps = defaultProps;

export default AdminList;
