import { createSelector } from 'reselect';

const selectRootState = (state) => state.get('admin');

const selectError = () => createSelector(
  selectRootState,
  (state) => state.get('error')
);

const selectLoading = () => createSelector(
  selectRootState,
  (state) => state.get('loading')
);

const selectLoggedIn = () => createSelector(
  selectRootState,
  (state) => state.get('loggedIn')
);

const selectUser = () => createSelector(
  selectRootState,
  (state) => state.get('user')
);

export {
  selectError,
  selectLoading,
  selectLoggedIn,
  selectUser
};
