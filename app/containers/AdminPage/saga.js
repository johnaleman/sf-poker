import firebase from 'firebase';
import { call, fork, put, take, takeEvery } from 'redux-saga/effects';

// actions
import {
  types,
  loginSuccess,
  loginFailure,
  logoutSuccess,
  logoutFailure,
  syncUser
} from './actions';

import rsf from 'utils/firebase/rsf';

const authProvider = new firebase.auth.GoogleAuthProvider();

function* loginSaga(action) {

  try {
    const formData = action.formData;
    const email = formData.email;
    const password = formData.password;

    const data = yield call(rsf.auth.signInWithEmailAndPassword, email, password);
    yield put(loginSuccess(data));
  } catch (error) {
    yield put(loginFailure(error));
  }
}

function * logoutSaga() {
  console.log('logoutSaga');

  try {
    const data = yield call(rsf.auth.signOut);
    yield put(logoutSuccess(data));
  } catch (error) {
    yield put(logoutFailure(error));
  }
}

function* syncUserSaga() {
  const channel = yield call(rsf.auth.channel);

  while (true) {
    const { user } = yield take(channel);

    if (user) {
      yield put(loginSuccess(user));
      yield put(syncUser(user));
    } else {
      yield put(syncUser(null));
    }
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* root() {
  yield fork(syncUserSaga);
  yield [
    takeEvery(types.LOGIN.REQUEST, loginSaga),
    takeEvery(types.LOGOUT.REQUEST, logoutSaga)
  ];
}
