/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';

// selectors
import { createStructuredSelector } from 'reselect';
import { selectError, selectLoading, selectLoggedIn, selectUser } from './selectors';

// redux
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import saga from './saga';
import { login, logout } from './actions';
import reducer from './reducer';

// layout
import { Page, Row, Column, BreakpointProvider } from 'hedron';
import Breakpoint from 'utils/breakpoint';

// child pages
import AdminVehiclePage from 'containers/AdminVehiclePage/Loadable';
import AdminServicePage from 'containers/AdminServicePage/Loadable';
import AdminAppointmentPage from 'containers/AdminAppointmentPage/Loadable';

// ui
import ButtonBase from 'components/ButtonBase';
import H1 from 'components/H1';
import messages from './messages';
import AdminLoginForm from './form';

import { NavLink } from 'react-router-dom';

export class AdminPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    // initial state
    this.state = {
      displayLoginForm: false,
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
  }

  componentWillUpdate(nextProps) {
    if (nextProps.user && nextProps.user !== this.props.user) {
      this.setState({ displayLoginForm: false });
    }
  }

  handleFormSubmit(form) {
    this.props.dispatchLogin(form);
  }

  handleLoginClick() {
    this.setState({ displayLoginForm: !this.state.displayLoginForm });
  }

  handleLogoutClick() {
    this.props.dispatchLogout();
  }

  render() {
    const { displayLoginForm } = this.state;
    const { error, loggedIn, user, match } = this.props;
    const userEmail = (loggedIn && user) ? user.email : '';

    // child routes
    const childServiceRoute = `${match.url}/service`;
    const childVehicleRoute = `${match.url}/vehicle`;
    const childAppointmentRoute = `${match.url}/appointment`;

    const formTitle = 'Login';

    return (
      <BreakpointProvider breakpoints={{ sm: Breakpoint.SM, md: Breakpoint.MD, lg: Breakpoint.LG }}>
        <Page width={`${Breakpoint.MAX_WIDTH}px`}>
          <Helmet>
            <title>Services Page</title>
            <meta name="description" content="Services page" />
          </Helmet>

          <Row>
            <Column>
              <H1>
                <FormattedMessage {...messages.header} />
              </H1>
              <h2>user: {userEmail}</h2>

              <ButtonBase onClick={this.handleLoginClick}>Login</ButtonBase>
              <ButtonBase onClick={this.handleLogoutClick}>Logout</ButtonBase>

              {user
                && (
                  <span>
                    <NavLink to={'/admin/service'}>
                      <ButtonBase>Service</ButtonBase>
                    </NavLink>

                    <NavLink to={'/admin/vehicle'}>
                      <ButtonBase>Vehicle</ButtonBase>
                    </NavLink>

                    <NavLink to={'/admin/appointment'}>
                      <ButtonBase>Appointment</ButtonBase>
                    </NavLink>
                  </span>
                )
              }
            </Column>
          </Row>

          {displayLoginForm
            && (
              <Row>
                <Column>
                  <AdminLoginForm
                    error={error}
                    title={formTitle}
                    handleFormSubmit={this.handleFormSubmit}
                  />
                </Column>
              </Row>
            )
          }

          {user
            && (
              <div>
                <Route
                  path={childServiceRoute}
                  component={AdminServicePage}
                />

                <Route
                  path={childVehicleRoute}
                  component={AdminVehiclePage}
                />

                <Route
                  path={childAppointmentRoute}
                  component={AdminAppointmentPage}
                />
              </div>
            )
          }
        </Page>
      </BreakpointProvider>
    );
  }
}

AdminPage.propTypes = {
  onChangeName: PropTypes.func,
  match: PropTypes.object,
};

export function mapDispatchToProps(dispatch) {
  return {
    dispatchLogin: (form) => {
      console.log('dispatchLogin');
      dispatch(login(form));
    },

    dispatchLogout: () => {
      console.log('dispatchLogout');
      dispatch(logout());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  error: selectError(),
  loading: selectLoading(),
  loggedIn: selectLoggedIn(),
  user: selectUser(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'admin', reducer });
const withSaga = injectSaga({ key: 'admin', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AdminPage);
