import { fromJS } from 'immutable';

import { types } from './actions';

// init state
const initialState = fromJS({
  name: 'AdminPage Reducer',
  loading: false,
  loggedIn: false,
  user: null,
  error: null
});

function reducer(state = initialState, action) {
  switch(action.type) {
    case types.LOGIN.REQUEST:
    case types.LOGOUT.REQUEST:
      return state.set('loading', true);

    case types.LOGIN.SUCCESS:
      console.log('[AdminPage reducer] LOGIN.SUCCESS');

      return state
        .set('loading', false)
        .set('loggedIn', true)
        .set('error', null);

    case types.LOGIN.FAILURE:
      return state
        .set('loading', false)
        .set('error', action.error);

    case types.LOGOUT.SUCCESS:
      console.log('[AdminPage reducer] LOGOUT.SUCCESS');

      return state
        .set('loading', false)
        .set('loggedIn', false)
        .set('error', null);

    case types.LOGOUT.FAILURE:
      return state.set('loading', false);

    case types.SYNC_USER:
      console.log('[AdminPage reducer] SYNC_USER', action.user);

      return state.set('user', action.user);

    default:
      return state;
  }
}

export default reducer;