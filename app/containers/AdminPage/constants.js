// create
export const CREATE_SERVICE = 'boilerplate/Service/CREATE_SERVICE';

// read
export const SYNC_SERVICE_LIST = 'boilerplate/Service/SYNC_SERVICE_LIST'; // fetch list on path change
export const FETCH_SERVICE_LIST = 'boilerplate/Service/FETCH_SERVICE_LIST';
export const FETCH_SERVICE_ITEM = 'boilerplate/Service/FETCH_SERVICE_ITEM';

// update
export const UPDATE_SERVICE_ITEM = 'boilerplate/Service/UPDATE_SERVICE_ITEM';

// destroy
export const DELETE_SERVICE_ITEM = 'boilerplate/Service/DELETE_SERVICE_ITEM';