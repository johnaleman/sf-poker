import React from 'react';
import PropTypes from 'prop-types';
import { fromJS } from 'immutable';
import cx from 'classnames';

import {
  addDays,
  addHours,
  format,
  differenceInHours,
  setHours,
  setMinutes,
  startOfToday,
} from 'date-fns';

import { AsYouType, isValidNumber } from 'libphonenumber-js';

// selectors
import { createStructuredSelector } from 'reselect';
import { selectLoading, selectLoggedIn, selectUser } from './selectors';

// redux
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';

// redux - service
import sagaService from 'containers/AdminPage/saga';
import reducerService from 'containers/AdminPage/reducer';

// components
import Form from 'components/Form';
import FormGroup from 'components/FormGroup';
import Input from 'components/Input';
import H3 from 'components/H3';

const FORM_DEFAULT = {
  // contact info
  email: '',
  password: ''
};

export class AdminLoginForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    // init state
    this.state = {
      model: FORM_DEFAULT,
      touched: {
        email: false,
        password: false
      }
    };

    // methods
    this.clearForm = this.clearForm.bind(this);
    this.getInputClassName = this.getInputClassName.bind(this);
    this.handleBlur = this.handleBlur.bind(this);

    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.populateMockData = this.populateMockData.bind(this);


    this.resetTouched = this.resetTouched.bind(this);
    this.updateSelectedItem = this.updateSelectedItem.bind(this);
    this.validate = this.validate.bind(this);
  }

  componentDidMount() {
    // this.populateMockData();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps && this.state.model.id !== nextProps.model.id) {

      this.setState({ model: nextProps.model, isDateLoading: true });
      this.renderDatePicker();
      return true;
    }

    return false;
  }

  clearForm() {
    this.setState({ model: FORM_DEFAULT }, () => {
      // complete
    });
  }

  handleEmailChange(event) {
    event.preventDefault();
    const value = event.target.value || '';

    // update
    this.updateSelectedItem('email', value);
  }

  handlePasswordChange(event) {
    event.preventDefault();
    const value = event.target.value || '';

    // update
    this.updateSelectedItem('password', value);
  }

  handleSubmit(event) {
    event.preventDefault();
    const { model } = this.state;
    const { handleFormSubmit } = this.props;

    // copy
    let copyModel = fromJS(model).toObject();

    handleFormSubmit(copyModel);

    this.setState({model: copyModel}, () => {
      // complete
      this.clearForm();
      this.resetTouched();
    });
  }

  populateMockData() {
    const email = 'johnaleman@gmail.com';
    const password = '';

    this.setState({
      model: {
        email,
        password
      }
    });
  }

  resetTouched() {
    this.setState({
      touched: {
        email: false,
        password: false,
      }
    });
  }

  updateSelectedItem(key, value) {
    return new Promise((resolve) => {
      // copy
      let copyModel = fromJS(this.state.model);
      copyModel = copyModel.set(key, value).toObject();

      // set state
      this.setState({model: copyModel}, () => {
        // complete
        resolve(copyModel);
      });
    });
  }

  handleBlur = (field) => () => {
    // copy
    let newForm = fromJS(this.state.touched);
    newForm = newForm.set(field, true).toObject();

    this.setState({ touched: newForm });
  };

  validate() {
    const { model } = this.state;

    // criteria
    const minChars = 3;
    const emailRegex = /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i;

    if (typeof model.email === 'undefined') {
      return {};
    }

    const isEmailValid = (model.email.length > minChars)
      && Boolean(model.email.match(emailRegex));

    const isPasswordValid = (model.password.length > minChars);

    // true means invalid
    return {
      email: !isEmailValid,
      password: !isPasswordValid,
    };
  }

  getInputClassName(field) {
    const { touched } = this.state;
    const errors = this.validate();
    const hasError = errors[field];
    const shouldShow = touched[field];

    const className = cx('form-group', {
      'error': hasError ? shouldShow : false
    });

    return className;
  }

  render() {
    const { model } = this.state;
    const { error, title } = this.props;
    console.log('error', error);

    const errors = this.validate();
    const isEnabled = !Object.keys(errors).some(x => errors[x]);

    const errorStyle = {
      color: 'red'
    };

    return (
      <div>
        <h4>{title}</h4>

        {error
          && <div className={'error'} style={errorStyle}>{error.message}</div>
        }

        <Form onSubmit={this.handleSubmit}>

          {/*contact info*/}
          <FormGroup className="form-group">
            <H3>Contact Info</H3>

            <Input
              type="email"
              className={this.getInputClassName('email')}
              name="email"
              placeholder="Email"
              value={model.email}
              // events
              onBlur={this.handleBlur('email')}
              onChange={this.handleEmailChange}
            />

            <Input
              type="password"
              className={this.getInputClassName('password')}
              name="password"
              placeholder="Password"
              value={model.password}
              // events
              onBlur={this.handleBlur('password')}
              onChange={this.handlePasswordChange}
            />

          </FormGroup>

          <Input type="submit" value="Submit" disabled={!isEnabled}/>
        </Form>
      </div>
    );
  }
}

AdminLoginForm.props = {
  error: PropTypes.object,
  title: PropTypes.string,
  model: PropTypes.object,
  handleFormSubmit: PropTypes.func.required
};

AdminLoginForm.defaultProps = {
  error: null,
  title: 'Create Appointment',
  model: {}
};

const mapStateToProps = createStructuredSelector({
  loading: selectLoading(),
  loggedIn: selectLoggedIn(),
  user: selectUser()
});

const withConnect = connect(mapStateToProps);
const withReducerService = injectReducer({ key: 'adminService', reducer: reducerService });
const withSagaService = injectSaga({ key: 'adminService', saga: sagaService });

export default compose(
  withReducerService,
  withSagaService,
  withConnect, // last
)(AdminLoginForm);