/*
 * SignUpPage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// saga
import AdminSaga from 'containers/AdminPage/saga';

// material-ui
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

import { withStyles } from '@material-ui/core/styles/index';
// import TextField from '@material-ui/core/TextField';

const styles = (theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
    },
    formContainer: {
        display: 'flex',
        flexDirection: 'column',
        border: '1px solid red',
        maxWidth: '600px',
    },
    h1: {
        display: 'block',
        fontSize: '1em',
    },
    margin: {
        margin: theme.spacing.unit,
    },
    withoutLabel: {
        marginTop: theme.spacing.unit,
    },
});

export default withStyles(styles)(class SignUpPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        classes: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
        };

        // bind methods
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(prop) {
        return (event) => {
            this.setState({ [prop]: event.target.value });
        };
    }

    handleSubmit() {
        console.log('handleSubmit');
    }

    render() {
        const { classes } = this.props;

        return (
          <div className={classes.root}>
            <h1 className={classes.h1}>Signup</h1>

            <div className={classes.formContainer}>
              <FormControl className={classNames(classes.margin)}>
                <InputLabel>Email</InputLabel>
                <Input
                  value={this.state.email}
                  onChange={this.handleChange('email')}
                />
              </FormControl>

              <FormControl className={classNames(classes.margin)}>
                <InputLabel>Password</InputLabel>
                <Input
                  type={'password'}
                  value={this.state.password}
                  onChange={this.handleChange('password')}
                />
              </FormControl>

              <Button variant="contained" color="primary" onClick={this.handleSubmit}>Submit</Button>
            </div>
          </div>
        );
    }
});
