import React from 'react';
import { FormattedMessage } from 'react-intl';
import { shallow } from 'enzyme';

import SignUpPage from '../index';
import messages from '../messages';

describe('<SignUpPage />', () => {
  it('should render the page message', () => {
    const renderedComponent = shallow(
      <SignUpPage />
    );
    expect(renderedComponent.contains(
      <FormattedMessage {...messages.header} />
    )).toEqual(true);
  });
});
