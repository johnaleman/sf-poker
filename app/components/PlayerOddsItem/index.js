import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Card from 'components/Card';
import Player from 'components/Player';
import Percent from 'components/Percent';

const styles = (theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        margin: theme.spacing.unit,
        marginBottom: '2.0em',
    },
    name: {},

    display: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },

    holeCards: {},
    percent: {
        marginLeft: 'auto',
        marginRight: '.2em',
    },
});

const PlayerOddsItem = (props) => {
    const {
        classes,
        name,
        holeCards,
        odds,
        ...rest
    } = props;

    const renderCards = () => {
        let card = '';

    // card 1
        card = holeCards.slice(0, 2);
        const card1 = {
            rank: card[0],
            suit: card[1],
        };

    // card 2
        card = holeCards.slice(2, 4);
        const cart2 = {
            rank: card[0],
            suit: card[1],
        };

        return (
          <div className={classes.holeCards}>
            <Card rank={card1.rank} suit={card1.suit} />
            <Card rank={cart2.rank} suit={cart2.suit} />
          </div>
        );
    };

    return (
      <div className={classes.container}>
        <Player
          className={classes.name}
          name={name}
        />

        <div className={classes.display}>
          {renderCards()}

          <div className={classes.percent}>
            <Percent
              value={odds}
            />
          </div>
        </div>
      </div>
    );
};

PlayerOddsItem.propTypes = {
    classes: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,
    holeCards: PropTypes.string.isRequired,
    odds: PropTypes.string.isRequired,
};

export default withStyles(styles)(PlayerOddsItem);
