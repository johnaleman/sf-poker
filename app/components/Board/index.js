import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Card from 'components/Card';

const styles = (theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
    },
    display: {
        display: 'flex',
        flexDirection: 'row',
    },
});

const Board = (props) => {
    const {
        classes,
        boardCards,
        ...rest
    } = props;

    const renderCards = () => {
        const holeCards = boardCards.split('');
        const numCards = holeCards.length / 2;

        let tempCard = '';
        const cardList = [];

        for (let i = 0; i < numCards; i++) {
            const min = i * 2;
            const max = min + 2;

            tempCard = holeCards.slice(min, max);

            tempCard = {
                rank: tempCard[0],
                suit: tempCard[1],
            };

            cardList.push(tempCard);
        }

        const render = cardList.map((card, i) => <Card key={i} rank={card.rank} suit={card.suit} />);

        return (
          <div className={classes.holeCards}>
            {render}
          </div>
        );
    };

    return (
      <div className={classes.container}>
        <h2>Board</h2>

        <div className={classes.display}>
          {renderCards()}
        </div>

      </div>
    );
};

Board.propTypes = {
    classes: PropTypes.object.isRequired,
    boardCards: PropTypes.string.isRequired,
};

export default withStyles(styles)(Board);
