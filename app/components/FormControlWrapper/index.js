import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';

const styles = (theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
    },
});

const FormControlWrapper = (props) => {
    const {
        classes,
        label,
        value,
        placeholder,
        // methods
        handleChange,
        ...rest
    } = props;

    const thisHandleChange = (event) => {
        //
        return handleChange(event);
    };

    return (
      <div className={classes.container}>
        <FormControl className={'FormControl'} fullWidth={true}>
          <InputLabel>{label}</InputLabel>
          <Input
            value={value}
            placeholder={placeholder}
            onChange={thisHandleChange}
          />
        </FormControl>
      </div>
    );
};

FormControlWrapper.propTypes = {
    classes: PropTypes.object.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    // methods
    handleChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(FormControlWrapper);
