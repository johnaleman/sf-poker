import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => ({
    container: {
        display: 'inline-flex',
        fontSize: '1.5em',
    },
});

const Percent = (props) => {
    const {
        classes,
        value,
        ...rest
    } = props;

    return (
      <div className={classes.container}>
        {value}
      </div>
    );
};

Percent.propTypes = {
    classes: PropTypes.object.isRequired,
    value: PropTypes.string.isRequired,
};

export default withStyles(styles)(Percent);
