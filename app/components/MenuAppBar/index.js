import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';


import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

const styles = {
    root: {
        flexGrow: 1,
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    navLink: {
        color: '#000',
        textDecoration: 'none',
    },
};

class MenuAppBar extends React.Component {
    constructor(props) {
        super(props);

        this.renderLoginToggle = this.renderLoginToggle.bind(this);
        this.renderMenuIcon = this.renderMenuIcon.bind(this);
    }

    state = {
        auth: true,
        anchorEl: null,
    };

    handleChange = (event, checked) => {
        this.setState({ auth: checked });
    };

    handleMenu = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    renderLoginToggle() {
        const { classes } = this.props;

        const { auth, anchorEl } = this.state;

        return (
          <FormGroup>
            <FormControlLabel
              control={
                <Switch
                  checked={auth}
                  onChange={this.handleChange}
                  aria-label="LoginSwitch"
                />
                  }
              label={auth ? 'Logout' : 'Login'}
            />
          </FormGroup>
        );
    }

    renderMenuIcon() {
        const { classes } = this.props;

        return (
          <IconButton
            className={classes.menuButton}
            color="inherit"
            aria-label="Menu"
            onClick={this.handleMenu}
          >
            <MenuIcon />
          </IconButton>
        );
    }

    render() {
        const { classes } = this.props;
        const { auth, anchorEl } = this.state;

        const title = 'Holdem\' Odds Calculator';

        const open = Boolean(anchorEl);

        // TODO: make component
        const LoginToggle = this.renderLoginToggle();
        const renderMenuItem = this.renderMenuIcon();

        return (
          <div className={classes.root}>
            <AppBar position="static">
              <Toolbar>
                <Typography
                  variant="title"
                  color="inherit"
                  className={classes.flex}
                >
                  {title}
                </Typography>
                {auth && (
                <div>
                  <IconButton
                    aria-owns={open ? 'menu-appbar' : null}
                    aria-haspopup="true"
                    color="inherit"
                  >
                    <AccountCircle />
                  </IconButton>

                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    open={open}
                    onClose={this.handleClose}
                  >
                    <MenuItem onClick={this.handleClose}>
                      <NavLink className={classes.navLink} to={'/'}>Home</NavLink>
                    </MenuItem>
                    <MenuItem onClick={this.handleClose}>
                      <NavLink className={classes.navLink} to={'/signup'}>Signup</NavLink>
                    </MenuItem>
                  </Menu>

                </div>
            )}
              </Toolbar>
            </AppBar>
          </div>
        );
    }
}

MenuAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuAppBar);
