import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => ({
    container: {
        display: 'flex',
    },
    name: {
        padding: theme.spacing.unit,
        fontSize: '1.5em',
    },
});

const Player = (props) => {
    const {
      classes,
      name,
      ...rest
    } = props;

    return (
      <div className={classes.container}>
        <div className={classes.name}>
          {name}
        </div>
      </div>
    );
};

Player.propTypes = {
    classes: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,
    email: PropTypes.string,
};

Player.defaultProps = {
    email: '',
};

export default withStyles(styles)(Player);
