import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
    },
});

const Component = (props) => {
    const {
        classes,
        ...rest
    } = props;

    return (
      <div className={classes.container}>
        Component
    </div>
    );
};

Component.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Component);
