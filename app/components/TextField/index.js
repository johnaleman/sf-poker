import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = (theme) => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
});

const TextFieldMargins = (props) => {
    const {
      classes,
      ...rest
  } = props;

    return (
      <TextField className={classes.textField} {...rest} />
    );
};

TextFieldMargins.propTypes = {
    classes: PropTypes.object.isRequired,
    defaultValue: PropTypes.string,
    helperText: PropTypes.string,
    label: PropTypes.string,
};

TextFieldMargins.defaultProps = {
    defaultValue: '',
    helperText: '',
    label: '',
};

export default withStyles(styles)(TextFieldMargins);
