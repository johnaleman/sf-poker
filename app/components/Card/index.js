import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => ({
    container: {
        border: '.2em solid orange',

        display: 'inline-flex',
        margin: theme.spacing.unit,
        padding: '1.1em .7em',

        fontSize: '1.3em',
        color: 'blue',
    },
    rank: {
        textTransform: 'uppercase',
    },
    suit: {
        textTransform: 'lowercase',
    },

});

const Card = (props) => {
    const {
        classes,
        ...rest
    } = props;

    return (
      <div className={classes.container}>
        <div className={classes.rank}>
          {rest.rank}
        </div>
        <div className={classes.suit}>
          {rest.suit}
        </div>
      </div>
    );
};

Card.propTypes = {
    classes: PropTypes.object.isRequired,
    rank: PropTypes.string.isRequired,
    suit: PropTypes.string.isRequired,
};

export default withStyles(styles)(Card);
