import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

class ComposedTextField extends React.Component {
    state = {
        name: '',
    };

    handleChange = event => {
        this.setState({ name: event.target.value });
    };

    render() {
        const { classes } = this.props;

        const label = 'AsKs 2d2h';
        const hasError = false;

        return (
          <div className={classes.container}>
            <FormControl className={classes.formControl} error={hasError} disabled={false} aria-describedby="name-error-text">
              <InputLabel htmlFor="name-error">{label}</InputLabel>
              <Input id="name-error" value={this.state.name} onChange={this.handleChange} />

              {hasError &&
                <FormHelperText id="name-error-text">Error</FormHelperText>
              }


            </FormControl>
          </div>
        );
    }
}

ComposedTextField.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ComposedTextField);
