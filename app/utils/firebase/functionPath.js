import { firebaseConfig } from './config';

const sendEmailPath = () => {
  return `${firebaseConfig.functionURL}/sendEmail`;
};

const sendSMSPath = () => {
  return `${firebaseConfig.functionURL}/sendSMS`;
};

export default {
  sendEmailPath,
  sendSMSPath
};